var express = require('express');
var app = express();
var db = require('./db');
var morgan = require('morgan')

app.use(morgan('dev'));

var AuthController = require('./auth/AuthController');
app.use('/api/auth', AuthController);

var FileController = require('./file/FileController');
app.use('/api/file', FileController);

module.exports = app;