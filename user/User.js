var config = require('../db');
var bcrypt = require('bcrypt-nodejs');
var Promise = require('bluebird');
const Sequelize = require('sequelize');

//Table columns
config.connectDB();

module.exports = {
  findById: function (id) {
    return (config.User.findOne({
      where: {
        id: id
      }
    }));
  },
  findByEmail: function (email) {
    return config.User.findOne({
      where: {
        email: email
      }
    });
  },
  create: function (data, password, callback) {
    return config.User.create({
      name: data.name,
      surname: data.surname,
      birthday: data.birthday,
      email: data.email,
      phone: data.phone,
      password: password
    })
  }
}