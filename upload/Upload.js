const path = require('path');
const storage = require('azure-storage');
const storageAccount = 'mydocumentsstorage';
const storageAccessKey = 'ktjA+8SMd3HJ+G/kL2Q/iByq8W/cw5pbODhW8o5oMqkj/KL4B/XvCJvXSAkBL+j7ZQ5TECpLFUQs6E4nuobc+A==';
const blobService = storage.createBlobService(storageAccount, storageAccessKey);


module.exports = {
    createContainer: function (containerName) {
        return new Promise((resolve, reject) => {
            blobService.createContainerIfNotExists(containerName, { publicAccessLevel: 'blob' }, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ message: `Container '${containerName}' criado` });
                }
            });
        });
    },

    upload: function (container, file) {
        return new Promise((resolve, reject) => {
            //var blobName = path.basename(file.path, path.extname(file.path));
            var blobName = file.name.replace(/^.*[\\\/]/, '');
            var options = {
                contentType: file.type,
                metadata: { fileName: file.name }
            };

            blobService.createBlockBlobFromLocalFile(container, blobName, file.path, options, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ message: `Itens no container '${container}':`, data: data });
                }
            });
        });
    },

    list: function (container) {
        return new Promise((resolve, reject) => {
            blobService.listBlobsSegmented(container, null, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ message: `Itens no container '${container}':`, data: data });
                }
            });
        });
    },

    download: function (container, blobName) {

        return new Promise((resolve, reject) => {
            blobService.getBlobToLocalFile(container, blobName, res, err => {
                if (err) {
                    reject(err);
                } else {
                    console.log(sourceFilePath, 'download')
                    resolve({ message: `Download do '${blobName}' completo`, data: sourceFilePath });
                }
            });
        });
    },

    deleteBlock: function (userI, blobName) {
        return new Promise((resolve, reject) => {
            blobService.deleteBlobIfExists(userID, blobName, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ message: `Bloco blob '${blobName}' deleted` });
                }
            });
        });
    },
    blobService: blobService
}