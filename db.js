const Sequelize = require('sequelize');

//Configure the DB connection
const sequelize = new Sequelize('DBMyDocuments', 'filipe.moises@mydocuments', '@mydocuments123@', {
    host: 'mydocuments.database.windows.net',
    port: 1433,
    dialect: 'mssql',
    encrypt: true,
    dialectOptions: {
        encrypt: true,
        requestTimeout: 30000 // timeout = 30 seconds
    }
});

//Method to create the tables
const userTable = sequelize.define('tb_user', {
    name: {
        type: Sequelize.STRING
    },
    surname: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    birthday: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },

    password: {
        type: Sequelize.STRING
    }
});


module.exports = {
    //Create the connection with DB
    connectDB: function () {
        userTable.sync({ force: false });
        sequelize
            .authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });
    },
    sequelize: sequelize,
    User: userTable
}
