Para excutar o projeto localmente é necessario realizar as configurações a seguir:

1-- Alterar arquivo db.js com as configurações da Base de dados desejada.
2-- Alterar arquivo config.js com a chave desejada para geração correta do token oauth
3-- Alterar arquivo Upload.js dentro da pasta upload com a chave e conta do Azure Storage desejada
4-- Alterar arquivo server.js substituir linha 2 para "var port = process.env.PORT || 3000;"
5-- Executar no terminal na pasta do projeto o comando npm install
6-- Executar no terminal na pasta do projeto o comando node server.js
