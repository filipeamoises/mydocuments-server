var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('../user/User');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');
var VerifyToken = require('./VerifyToken');


router.get('/ping', function (req, res) {
    res.status(200).send(pong);
});
//Endpoint to register a user
router.post('/register', async function (req, res) {
    //hash the password to store on Database
    console.log(req.body)
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    try {
        var user
        await User.findByEmail(req.body.email).then(result => user = result.get({ plain: true })).catch(err => console.log(err));
        if (user == null) {
            await User.create(req.body, hashedPassword).then(result => user = result.get({ plain: true })).catch(err => console.log(err));
            // create a token
            var token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 604800 // expires in one week
            });
            res.status(200).send({ auth: true, token: token, userId: user.id });
        } else {
            res.status(412).send({
                title: "Autenticação",
                detail: "E-mail já cadastrado",
                status: "Precondition failed",
                type: "auth",
                instance: "Auth error",
                code: "412"
            });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Ocorreu um erro ao registrar o usuario");
    }
});

// Returns the name of user if his token is valide
router.get('/me', VerifyToken, function (req, res) {
    var token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'O token de acesso não foi recebido' });

    jwt.verify(token, config.secret, async function (err, decoded) {
        if (err) return res.status(500).send({ auth: false, message: 'Ocorreu um erro ao validar o token' });

        var user;
        await User.findById(decoded.id).then(
            result => user = result.get({ plain: true })).catch(err => {
                console.log(err);
                return res.status(500).send("Ocorreu um erro ao tentar achar o usuario")
            });

        if (!user) return res.status(404).send("Nenhum usuario encontrado.");

        res.status(200).send(user.name);
    });
});

//Endpoint to Login
router.post('/login', async function (req, res) {
    var user
    await User.findByEmail(req.body.email).then(
        result => user = result.get({ plain: true })).catch(err => {
            console.log(err);
            return res.status(500).send("Ocorreu um erro ao realizar o login")
        });

    if (!user) return res.status(404).send('No user found.');
    //Compare the passaword from user database and the passaword from requisition
    var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
    var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 604800 // expires in one week
    });
    res.status(200).send({ auth: true, token: token, userId: user.id });
});

module.exports = router;