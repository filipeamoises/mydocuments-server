var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');
var upload = require('../upload/Upload');
var verifyToken = require('../auth/VerifyToken');

//Endpoint to upload file
router.post('/upload', multipartMiddleware, verifyToken, async function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    var fileReq = req.files.file;
    var container = 'container' + req.get('userId');

    //Create container on azure storage if dont exist
    await upload.createContainer(container);
    //Upload file to container on azure storage
    await upload.upload(container, fileReq).then((result, reject) => {
        if (reject) {
            res.status(500).send("Ocorreu um erro ao realizar o upload do arquivo.");
        } else {
            res.status(200).send();
        }
    });
});

//Endpoint to list files in cloud
router.get('/list/:id', verifyToken, async function (req, res) {
    var container = 'container' + req.params.id;
    await upload.createContainer(container).then(result => console.log(result)).catch(err => res.status(500).send(err));
    await upload.list(container).then(result => {
        res.status(200).send(result.data.entries)
    });
});

//Endpoint to download files
router.get('/download/:userId/:fileName',  async function (req, res) {
    var container = 'container' + req.params.userId;
    var fileName =  req.params.fileName;

    upload.blobService.getBlobProperties(
        container,
        fileName,
        function (err, properties, status) {
            if (err) {
                res.send(502, "Error fetching file: %s", err.message);
            } else if (!status.isSuccessful) {
                res.send(404, "The file %s does not exist", fileName);
            } else {
                res.header('Content-Type', properties.contentType);
                upload.blobService.createReadStream(container, fileName).pipe(res);
            }
        });

});

module.exports = router;